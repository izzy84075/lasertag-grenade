#include "BUTTON.h"

volatile uint16_t BUTTON_currentStateTimeMS = 0;
volatile bool BUTTON_lastState = false;

volatile bool BUTTON_touched = false;
volatile bool BUTTON_held = false;
volatile bool BUTTON_released = false;
volatile bool BUTTON_touchAcked = false;
volatile bool BUTTON_holdAcked = false;
volatile bool BUTTON_releaseAcked = false;

void BUTTON_Tick1ms(void) {
	bool tempSwitchState = BUTTON_READ();
	
	if(BUTTON_lastState != tempSwitchState) {
		BUTTON_lastState = tempSwitchState;
		BUTTON_currentStateTimeMS = 0;
	}
	
	BUTTON_currentStateTimeMS++;
	
	if(tempSwitchState) {
		//Currently touched
		
		if(BUTTON_releaseAcked) {
			BUTTON_released = false;
			BUTTON_releaseAcked = false;
		}
		
		if(BUTTON_currentStateTimeMS > BUTTON_TOUCH_DEBOUNCE_TIME_MS) {
			BUTTON_touched = true;
		}
		
		if(BUTTON_currentStateTimeMS > BUTTON_HOLD_DEBOUNCE_TIME_MS) {
			BUTTON_held = true;
		}
		
	} else {
		//Currently not touched
		
		//Clear ACKs
		if(BUTTON_touchAcked) {
			BUTTON_touched = false;
			BUTTON_touchAcked = false;
		}
		if(BUTTON_holdAcked) {
			BUTTON_held = false;
			BUTTON_holdAcked = false;
		}
		
		if(BUTTON_currentStateTimeMS > BUTTON_RELEASE_DEBOUNCE_TIME_MS) {
			BUTTON_released = true;
		}
	}
}

bool BUTTON_CurrentlyTouched(void) {
	return BUTTON_READ();
}

bool BUTTON_Touched(void) {
	if(BUTTON_touchAcked) {
		return false;
	} else {
		return BUTTON_touched;
	}
}

bool BUTTON_Released(void) {
	if(BUTTON_releaseAcked) {
		return false;
	} else {
		return BUTTON_released;
	}
}

bool BUTTON_Held(void) {
	 if(BUTTON_holdAcked) {
		 return false;
	 } else {
		 return BUTTON_held;
	 }
}

void BUTTON_AckTouch(void) {
	BUTTON_touchAcked = true;
}

void BUTTON_AckRelease(void) {
	BUTTON_releaseAcked = true;
}

void BUTTON_AckHold(void) {
	BUTTON_holdAcked = true;
}

void BUTTON_AckAll(void) {
	BUTTON_touchAcked = true;
	BUTTON_releaseAcked = true;
	BUTTON_holdAcked = true;
}
