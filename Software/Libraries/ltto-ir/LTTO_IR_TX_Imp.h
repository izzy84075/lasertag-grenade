#ifndef __LTTO_IR_TX_IMP_H__
#define __LTTO_IR_TX_IMP_H__

#include <stdint.h>
#include <stdbool.h>

#include "../../Drivers/stm32f0_GPIO.h"

extern volatile bool LTTO_IR_TX_LED_Enable3and4;
extern volatile bool LTTO_IR_TX_LED_Enable1and2;
extern volatile bool LTTO_IR_TX_LED_Enable5and7;
extern volatile bool LTTO_IR_TX_LED_Enable6and8;

//#define LTTO_IR_TX_IREnable()
static inline void LTTO_IR_TX_IREnable(void) {
    uint32_t tempPinMask = 0;
    if(LTTO_IR_TX_LED_Enable3and4) {
        tempPinMask |= 0x0002;
    }
    if(LTTO_IR_TX_LED_Enable1and2) {
        tempPinMask |= 0x0004;
    }
    if(LTTO_IR_TX_LED_Enable5and7) {
        tempPinMask |= 0x0008;
    }
    if(LTTO_IR_TX_LED_Enable6and8) {
        tempPinMask |= 0x0010;
    }
    STM_GPIO_SetPinState(GPIO_PORTA, tempPinMask, true);
}

//#define LTTO_IR_TX_IRDisable()
static inline void LTTO_IR_TX_IRDisable(void) {
    //Turn off all the LEDs
    STM_GPIO_SetPinState(GPIO_PORTA, (GPIO_PIN1 | GPIO_PIN2 | GPIO_PIN3 | GPIO_PIN4), false);
}

#endif
