#include "LED_Animate.h"

volatile uint8_t LED_timerMSRemaining = 0;
volatile uint8_t LED_timerMSStarting = 0;
volatile uint8_t LED_mode = 0;
volatile uint8_t LED_state = 0;
volatile uint32_t LED_tempLevel = 0;

#define LED_SLOW_STEP_MS	120
#define LED_MEDIUM_STEP_MS	80
#define LED_FAST_STEP_MS	40

uint8_t LED_breathe[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	16,
	32,
	48,
	64,
	80,
	96,
	112,
	128,
	144,
	160,
	176,
	192,
	208,
	224,
	240,
	255,
	240,
	225,
	208,
	192,
	176,
	160,
	144,
	128,
	112,
	96,
	80,
	64,
	48,
	32,
	16,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
const uint8_t LED_BREATHE_LENGTH = (sizeof(LED_breathe)/sizeof(LED_breathe[0]));

#include "../../Drivers/stm32f0_TIM.h"

void LED_IMP_SetLed(uint8_t pwmValue) {
	STM_TIM_TimerChannelOutputPWM_UpdatePWMValue(TIM_MODULE_1, TIM_CHANNEL_2, (255-pwmValue), 255);
}

void LED_Tick1ms(void) {
	if(LED_timerMSRemaining) {
		LED_timerMSRemaining--;
		if(!LED_timerMSRemaining) {
			switch(LED_mode) {
				case 0:
					//Do nothing!
					break;
				case 1:
					//Breathe
					LED_tempLevel = LED_breathe[LED_state];
					LED_state++;
					LED_state %= LED_BREATHE_LENGTH;
					break;
				case 2:
					//Blink
					if(LED_state != 0) {
						//Turn LED off
						LED_tempLevel = 255;
						LED_state = 0;
					} else {
						//Turn LED on
						LED_tempLevel = 0;
						LED_state = 1;
					}
					break;
			}
			//LED_IMP_SetLed(temp);
			//STM_TIM_TimerChannelOutputPWM_UpdatePWMValue(TIM_MODULE_1, TIM_CHANNEL_2, (255-temp), 255);
			TIM1->CCR2 = (255-LED_tempLevel);
			LED_timerMSRemaining = LED_timerMSStarting;
		}
	}
}

void LED_Speed_Slow(void) {
	LED_timerMSStarting = LED_SLOW_STEP_MS;
}

void LED_Speed_Medium(void) {
	LED_timerMSStarting = LED_MEDIUM_STEP_MS;
}

void LED_Speed_Fast(void) {
	LED_timerMSStarting = LED_FAST_STEP_MS;
}

void LED_Speed_Custom(uint8_t ledTickMS) {
	LED_timerMSStarting = ledTickMS;
}

void LED_Breathe(void) {
	LED_mode = 0;
	LED_state = 0;
	LED_mode = 1;
	LED_timerMSRemaining = LED_timerMSStarting;
}

void LED_Blink(void) {
	LED_mode = 2;
	LED_timerMSRemaining = LED_timerMSStarting;
}

void LED_Off(void) {
	LED_mode = 0;
	TIM1->CCR2 = (255-0);
	LED_timerMSRemaining = 0;
}

void LED_On(void) {
	LED_mode = 0;
	TIM1->CCR2 = (255-255);
	LED_timerMSRemaining = 0;
}

void LED_SetLevel(uint8_t pwmValue) {
	LED_mode = 0;
	LED_IMP_SetLed(pwmValue);
	LED_timerMSRemaining = 0;
}
