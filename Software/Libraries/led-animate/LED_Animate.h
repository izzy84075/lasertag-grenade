#ifndef __LED_ANIMATE_H__
#define __LED_ANIMATE_H__

#include <stdint.h>
#include <stdbool.h>

#include "LED_Animate_Conf.h"

void LED_Tick1ms(void);

void LED_Speed_Slow(void);
void LED_Speed_Medium(void);
void LED_Speed_Fast(void);
void LED_Speed_Custom(uint8_t ledTickMS);

void LED_Breathe(void);
void LED_Blink(void);
void LED_Off(void);
void LED_On(void);
void LED_SetLevel(uint8_t pwmValue);

#endif
