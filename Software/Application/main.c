#include <stdint.h>
#include <stdbool.h>

#include "../Drivers/stm32f0xx.h"
#include "../Drivers/stm32f0_GPIO.h"
#include "../Drivers/stm32f0_TIM.h"

#include "../Libraries/ltto-ir/LTTO_IR_DataStructures.h"
#include "../Libraries/ltto-ir/LTTO_IR_RX_AutoConvert.h"
#include "../Libraries/ltto-ir/LTTO_IR_RX.h"
#include "../Libraries/ltto-ir/LTTO_IR_TX.h"
#include "../Libraries/button/BUTTON.h"

#include "../Libraries/led-animate/LED_Animate.h"

volatile uint32_t sysTime = 0;

volatile bool LTTO_IR_TX_LED_Enable1and2 = true;
volatile bool LTTO_IR_TX_LED_Enable3and4 = false;
volatile bool LTTO_IR_TX_LED_Enable5and7 = false;
volatile bool LTTO_IR_TX_LED_Enable6and8 = false;

#define IR_RX_PORT_IN		GPIOA->IDR
#define IR_RX_PIN			0x0040
#define IR_RX_READ()		((IR_RX_PORT_IN & IR_RX_PIN) == IR_RX_PIN)

void ServiceLoops(void) {

}

#define IR_RX_SENSOR_TICKS_GAVE_UP  10000

LTTO_IR_SIGNATURE_t tempSignature;
volatile int tempPulseMS[22];
volatile int tempPulseCount;

void SysTick_Handler(void) {
	//0.2ms intervals
	static uint16_t msCounter = 0;
	static bool IR_RX_LastState = false;
	static uint16_t IR_RX_SensorTicks = 0;

	bool IR_RX_currentState = IR_RX_READ();

	//Handle IR reception
	//If the state is the same as last time
	if(IR_RX_currentState == IR_RX_LastState) {
		//If this pulse has gone longer than 10ms, and we haven't already decided we've timed out...
		if((IR_RX_SensorTicks / LTTO_IR_RX_TICKS_PER_MS) > 10 && IR_RX_SensorTicks != IR_RX_SENSOR_TICKS_GAVE_UP) {
			//Pass it into the RX processor
			//Invert the sensor state, since IR receivers are active-low
			LTTO_IR_RX_AC_NewIRPulseReceived(IR_RX_SensorTicks, !IR_RX_LastState);
			//And mark this pulse as having already been processed
			IR_RX_SensorTicks = IR_RX_SENSOR_TICKS_GAVE_UP;
		} else {
			//Otherwise, increment the counter
			IR_RX_SensorTicks++;
		}
	} else {
		//If we haven't already processed this pulse due to timing out...
		if(IR_RX_SensorTicks != IR_RX_SENSOR_TICKS_GAVE_UP) {
			//Pass it into the processor.
			//Invert the state, since IR receivers are active-low.
			LTTO_IR_RX_AC_NewIRPulseReceived(IR_RX_SensorTicks, !IR_RX_LastState);
		}
		//And reset the timer
		IR_RX_SensorTicks = 0;
		//And store the current state of the pin
		IR_RX_LastState = !IR_RX_LastState;
	}

	//Work towards a 1ms time interval
	if(!msCounter) {
		msCounter = LTTO_IR_RX_TICKS_PER_MS;

		sysTime++;

		LTTO_IR_TX_Tick1MS();
		LED_Tick1ms();
		BUTTON_Tick1ms();
	}
	msCounter--;
}

void Delay(uint32_t msDelay) {
	uint32_t targetMS = (sysTime + msDelay + 1);

	while(targetMS > sysTime) {
		ServiceLoops();
	}
}

void MCU_Init(void) {
	//Turn on the power keepalive pin first
	STM_GPIO_EnablePort(GPIO_PORTA);
	STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN0, GPIO_MODE_OUTPUT);
	STM_GPIO_SetPinState(GPIO_PORTA, GPIO_PIN0, true);

	//Set up the clock
	STM_RCC_ClkInit();

	//Set up the rest of the pins
	//Port A
	//Output lows...
	STM_GPIO_SetPinMode(GPIO_PORTA, (GPIO_PIN1 | GPIO_PIN2 | GPIO_PIN3 | GPIO_PIN4 | GPIO_PIN5 | GPIO_PIN7), GPIO_MODE_OUTPUT);
	STM_GPIO_SetPinState(GPIO_PORTA, (GPIO_PIN1 | GPIO_PIN2 | GPIO_PIN3 | GPIO_PIN4 | GPIO_PIN5 | GPIO_PIN7), false);
	//Output highs...
	STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN9, GPIO_MODE_OUTPUT);
	STM_GPIO_SetPinState(GPIO_PORTA, GPIO_PIN9, true);
	//Input float
	STM_GPIO_SetPinMode(GPIO_PORTA, (GPIO_PIN6), GPIO_MODE_INPUT);
	//Port B
	//Output Low
	STM_GPIO_EnablePort(GPIO_PORTB);
	STM_GPIO_SetPinMode(GPIO_PORTB, GPIO_PIN1, GPIO_MODE_OUTPUT);
	STM_GPIO_SetPinState(GPIO_PORTB, GPIO_PIN1, false);

	//Port F
	//Input Pull Low
	STM_GPIO_EnablePort(GPIO_PORTF);
	STM_GPIO_SetPinMode(GPIO_PORTF, GPIO_PIN0, GPIO_MODE_INPUT);
	STM_GPIO_SetPinPullMode(GPIO_PORTF, GPIO_PIN0, GPIO_PULL_UP);

	//Set up our 38KHz carrier generation
	STM_TIM_EnableTimer(TIM_MODULE_14, 76000);
	STM_TIM_ConfigureTimerChannelOutputCompare(TIM_MODULE_14, TIM_CHANNEL_1, TIM_CHANNEL_OUTPUT_COMPARE_MODE_TOG_OUTPUT, TIM_CHANNEL_OUTPUT_POLARITY_ACTIVE_HIGH, 0);
	STM_TIM_RunTimer(TIM_MODULE_14);
	//Set the IR carrier output pin to hook to the timer for generating the carrier.
	STM_GPIO_SetPinAlternateFunction(GPIO_PORTB, GPIO_PIN1, 0);
	STM_GPIO_SetPinMode(GPIO_PORTB, GPIO_PIN1, GPIO_MODE_ALTERNATE_FUNCTION);

	//Set up a PWM timer
	STM_TIM_EnableTimer(TIM_MODULE_1, 100000);
	STM_TIM_PauseTimer(TIM_MODULE_1);
	STM_TIM_ConfigureTimerChannelOutputPWM(TIM_MODULE_1, TIM_CHANNEL_2, TIM_CHANNEL_OUTPUT_POLARITY_ACTIVE_HIGH);
	STM_TIM_TimerChannelOutputPWM_UpdatePWMValue(TIM_MODULE_1, TIM_CHANNEL_2, (255-192), 255);
	STM_TIM_RunTimer(TIM_MODULE_1);
	//Set the PWM output pin to hook to the timer
	STM_GPIO_SetPinAlternateFunction(GPIO_PORTA, GPIO_PIN9, 2);
	STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN9, GPIO_MODE_ALTERNATE_FUNCTION);

	//Set up our 5KHz timebase
	SysTick_Config( (SystemCoreClock/5000) );
	NVIC_SetPriority(SysTick_IRQn, 0);
	NVIC_EnableIRQ(SysTick_IRQn);
}

uint8_t currentMode = 0;

int main(void) {
	MCU_Init();

	LED_Speed_Slow();
	LED_Breathe();

	Delay(1000);

	while(BUTTON_CurrentlyTouched()) {
		ServiceLoops();
	}

	BUTTON_AckAll();

	while(1) {
		ServiceLoops();

		switch(currentMode) {
		case 0:
			//Wait for a shot or a zone signal
			if(LTTO_IR_RX_AC_NewSingleSignatureReady()) {
				LTTO_IR_RX_AC_GetSingleSignature(&tempSignature);

				if(tempSignature.signatureType == LTTO_IR_SIGNATURE_TYPE_TAG) {
					//Save it and move on to the waiting
					//And let's go ahead and boost the damage, 'cus that's fun.
					tempSignature.data |= 0x03;
					LED_Speed_Fast();
					currentMode++;
					BUTTON_AckAll();
				} else if(tempSignature.signatureType == LTTO_IR_SIGNATURE_TYPE_LTTO_BEACON) {
					//Check whether it's a Zone or Medic signature
					uint8_t temp = ((tempSignature.data & 0x7C) >> 2);
					if( (temp < 8) && (temp > 3) ) {
						//Zone or base signature, move on
						LED_Speed_Fast();
						currentMode++;
						BUTTON_AckAll();
					}
				}

				LTTO_IR_RX_AC_Clear();
			}
			break;
		case 1:
			//Wait for a button hold to arm
			if(BUTTON_Held()) {
				currentMode++;
				LED_On();
				BUTTON_AckHold();
			}
			break;
		case 2:
			//Start the countdown
			Delay(1000);
			LED_Blink();
			LED_Speed_Slow();
			Delay(3333);
			LED_Speed_Medium();
			Delay(3333);
			LED_Speed_Fast();
			Delay(2333);
			LED_On();
			Delay(1000);
			currentMode++;
			break;
		case 3:
			LTTO_IR_TX_LED_Enable6and8 = false;
			LTTO_IR_TX_LED_Enable1and2 = true;
			LTTO_IR_TX_QueueSignature(&tempSignature);
			while(LTTO_IR_TX_Busy()) {
				ServiceLoops();
			}

			LTTO_IR_TX_LED_Enable1and2 = false;
			LTTO_IR_TX_LED_Enable3and4 = true;
			LTTO_IR_TX_QueueSignature(&tempSignature);
			while(LTTO_IR_TX_Busy()) {
				ServiceLoops();
			}

			LTTO_IR_TX_LED_Enable3and4 = false;
			LTTO_IR_TX_LED_Enable5and7 = true;
			LTTO_IR_TX_QueueSignature(&tempSignature);
			while(LTTO_IR_TX_Busy()) {
				ServiceLoops();
			}

			LTTO_IR_TX_LED_Enable5and7 = false;
			LTTO_IR_TX_LED_Enable6and8 = true;
			LTTO_IR_TX_QueueSignature(&tempSignature);
			while(LTTO_IR_TX_Busy()) {
				ServiceLoops();
			}

			currentMode++;
			break;
		case 4:
			//Turn off the charge pump
			Delay(1000);
			STM_GPIO_SetPinState(GPIO_PORTA, GPIO_PIN0, false);
			break;
		}
	}

	return 0;
}
