#include "stm32f0_SPI.h"

#if defined(stm32f030x6)
void STM_SPI_ConfigPins(void) {
    if(SPI1_MOSI_PIN == SPI1_MOSI_GPIOA7) {
        STM_GPIO_SetPinAlternateFunction(GPIO_PORTA, GPIO_PIN7, 0);
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN7, GPIO_MODE_ALTERNATE_FUNCTION);
    }
    if(SPI1_MISO_PIN == SPI1_MISO_GPIOA6) {
        STM_GPIO_SetPinAlternateFunction(GPIO_PORTA, GPIO_PIN6, 0);
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN6, GPIO_MODE_ALTERNATE_FUNCTION);
    }
    if(SPI1_SCK_PIN == SPI1_SCK_GPIOA5) {
        STM_GPIO_SetPinAlternateFunction(GPIO_PORTA, GPIO_PIN5, 0);
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN5, GPIO_MODE_ALTERNATE_FUNCTION);
    }
}
void STM_SPI_DeconfigPins(void) {
    if(SPI1_MOSI_PIN == SPI1_MOSI_GPIOA7) {
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN7, GPIO_MODE_ANALOG);
    }
    if(SPI1_MISO_PIN == SPI1_MISO_GPIOA6) {
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN6, GPIO_MODE_ANALOG);
    }
    if(SPI1_SCK_PIN == SPI1_SCK_GPIOA5) {
        STM_GPIO_SetPinMode(GPIO_PORTA, GPIO_PIN5, GPIO_MODE_ANALOG);
    }
}
#endif

void STM_SPI_Init(void) {
    //Turn on the clock to SPI1
    STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_SPI1);
    
    //Set up the GPIO pins
    STM_SPI_ConfigPins();
    
    //Set the clock divider
    SPI1->CR1 = ( (SPI1->CR1 & ~(SPI_CR1_BR_Msk)) | (SPI1_CLK_SRC << SPI_CR1_BR_Pos) );

    //Set to Master mode
    SPI1->CR1 |= SPI_CR1_MSTR;

    //Set the CPOL/CPHA mode.
    SPI1->CR1 = ( (SPI1->CR1 & ~((SPI_CR1_CPOL_Msk | SPI_CR1_CPHA_Msk))) | (SPI1_SPI_MODE << SPI_CR1_CPHA_Pos) );

    //Set the frame size to 8 bits
    SPI1->CR2 = (7 << SPI_CR2_DS_Pos);

    //And enable the SPI peripheral
    SPI1->CR1 |= SPI_CR1_SPE;
}

void STM_SPI_DeInit(void) {
    //Reset the associated pins
    STM_SPI_DeconfigPins();
    //We have a handy reset function. Use it to get the peripheral config back to default.
    STM_RCC_Peripheral_Reset(RCC_PERIPHERAL_SPI1);
    //And turn off the peripheral.
    STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_SPI1);
}

void STM_SPI_TXRX(uint8_t TXdata[], uint8_t RXdata[], uint16_t dataCount) {
    uint16_t i = 0;
    //Do the transfer!
    for(i = 0;i < dataCount;i++) {
        //Put byte into buffer
        SPI1->DR = TXdata[i];
        
        //Wait for the transfer to finish
        while( (SPI1->SR & SPI_SR_BSY) ) {
            //Wait!
        }

        //Read in the RX'd byte
        RXdata[i] = (uint8_t)((SPI1->DR));
    }
}
