#include "stm32f0_TIM.h"

TIM_TypeDef* STM_TIM_GetTimerHandle(TIM_MODULES_t whichModule) {
    switch(whichModule) {
        default:
            return 0;
            break;
        #if SELECTED_CHIP_HAS_TIM1 == true
        case TIM_MODULE_1:
            return (TIM_TypeDef*)(TIM1_BASE);
            break;
        #endif
        #if SELECTED_CHIP_HAS_TIM14 == true
        case TIM_MODULE_14:
            return (TIM_TypeDef*)(TIM14_BASE);
            break;
        #endif
    }
}

TIM_TypeDef* STM_TIM_GetTimerHandleIfChannelPresent(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel) {
    switch(whichModule) {
        default:
            return 0;
            break;
        #if SELECTED_CHIP_HAS_TIM1 == true
        case TIM_MODULE_1:
            if(whichChannel < SELECTED_CHIP_NUM_TIM1_CHANNELS) {
                return (TIM_TypeDef*)(TIM1_BASE);
            } else {
                return 0;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_TIM14 == true
        case TIM_MODULE_14:
            if(whichChannel < SELECTED_CHIP_NUM_TIM14_CHANNELS) {
                return (TIM_TypeDef*)(TIM14_BASE);
            } else {
                return 0;
            }
            break;
        #endif
    }
}

void STM_TIM_EnableTimerClock(TIM_MODULES_t whichModule) {
    switch(whichModule) {
        default:
            break;
        #if SELECTED_CHIP_HAS_TIM1 == true
        case TIM_MODULE_1:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_TIM1);
            break;
        #endif
        #if SELECTED_CHIP_HAS_TIM14 == true
        case TIM_MODULE_14:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_TIM14);
            break;
        #endif
    }
}

void STM_TIM_ResetTimerModule(TIM_MODULES_t whichModule) {
    switch(whichModule) {
        default:
            break;
        #if SELECTED_CHIP_HAS_TIM1 == true
        case TIM_MODULE_1:
            STM_RCC_Peripheral_Reset(RCC_PERIPHERAL_TIM1);
            break;
        #endif
        #if SELECTED_CHIP_HAS_TIM14 == true
        case TIM_MODULE_14:
            STM_RCC_Peripheral_Reset(RCC_PERIPHERAL_TIM14);
            break;
        #endif
    }
}

void STM_TIM_DisableTimerClock(TIM_MODULES_t whichModule) {
    switch(whichModule) {
        default:
            break;
        #if SELECTED_CHIP_HAS_TIM1 == true
        case TIM_MODULE_1:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_TIM1);
            break;
        #endif
        #if SELECTED_CHIP_HAS_TIM14 == true
        case TIM_MODULE_14:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_TIM14);
            break;
        #endif
    }
}

//Will return the "real" frequency that the timer is running at
//TODO: FIXME: This only allows down to 1Hz, 16-bit timers could go down to ~0.01Hz with the last few prescaler steps.
uint32_t STM_TIM_EnableTimer(TIM_MODULES_t whichModule, uint32_t targetFrequencyHz) {
    uint32_t realFrequencyHz = 0;
    
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandle(whichModule);
    if(timerHandle == 0) {
        //Timer module doesn't exist on this hardware
        return realFrequencyHz;
    }

    //Turn on the clock to the peripheral
    STM_TIM_EnableTimerClock(whichModule);

    //Need to calculate the prescaler and reload value for the given target frequency.
    //Raw number of clock ticks to get that frequency
    uint32_t timerTicksForFrequency = (SystemCoreClock / targetFrequencyHz);
    //The reload value we'll need to use
    uint32_t tempReload = 0;
    //And the prescaler value to use
    uint32_t tempDivisor = 1;
    if( timerTicksForFrequency <= 65536 ) {
        //Don't need to divide the clock, leave the divisor at 1.
        tempReload = timerTicksForFrequency;
    } else {
        while(timerTicksForFrequency > 65536) {
            tempDivisor++;
            timerTicksForFrequency = ((SystemCoreClock / tempDivisor) / targetFrequencyHz);
        }
        tempReload = timerTicksForFrequency;
    }
    //Back-calculate the real frequency we're getting
    realFrequencyHz = (SystemCoreClock / (tempReload*tempDivisor));
    //Prescaler and reload registers need to be one below the actual value
    tempReload--;
    tempDivisor--;

    //Start configuring the peripheral

    //Make sure we're not running the timer counter at the moment
    timerHandle->CR1 &= ~(TIM_CR1_CEN);

    //Set the auto-reload register
    timerHandle->ARR = tempReload;
    //Set the prescaler
    timerHandle->PSC = tempDivisor;
    //Reset the counter
    timerHandle->CNT = 0;

    //Make sure the timer will continue running forever and count up
    timerHandle->CR1 &= ~(TIM_CR1_UDIS | TIM_CR1_OPM | TIM_CR1_DIR);

    //Clear some advanced registers
    timerHandle->CR2 = 0;
    timerHandle->SMCR = 0;

    //And start running the timer
    timerHandle->CR1 |= TIM_CR1_CEN;

    return realFrequencyHz;
}

void STM_TIM_DisableTimer(TIM_MODULES_t whichModule) {
    //Try to get the handle for this module
    if(STM_TIM_GetTimerHandle(whichModule) == 0) {
        //Timer module doesn't exist on this hardware
        return;
    }

    //Reset the timer module registers
    STM_TIM_ResetTimerModule(whichModule);

    //Turn off the clock
    STM_TIM_DisableTimerClock(whichModule);
}

void STM_TIM_RunTimer(TIM_MODULES_t whichModule) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandle(whichModule);
    if(timerHandle == 0) {
        //Timer module doesn't exist on this hardware
        return;
    }

    //Start running the timer
    timerHandle->CR1 |= TIM_CR1_CEN;
}

void STM_TIM_PauseTimer(TIM_MODULES_t whichModule) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandle(whichModule);
    if(timerHandle == 0) {
        //Timer module doesn't exist on this hardware
        return;
    }

    //Stop running the timer
    timerHandle->CR1 &= ~(TIM_CR1_CEN);
}

void STM_TIM_ConfigureTimerChannelInput(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel, TIM_CHANNEL_INPUTS_t whichInput, TIM_CHANNEL_INPUT_ACTIVE_EDGES_t whichEdges, TIM_CHANNEL_INPUT_PRESCALERS_t prescaler, TIM_CHANNEL_INPUT_FILTER_CLK_SRCS_AND_NUM_TICKS_t inputFilterSetting) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }


}

void STM_TIM_ConfigureTimerChannelOutputCompare(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel, TIM_CHANNEL_OUTPUT_COMPARE_MODES_t whichMode, TIM_CHANNEL_OUTPUT_POLARITIES_t whichPolarity, uint16_t triggerValue) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }

    switch(whichChannel) {
        case TIM_CHANNEL_1:
            //Disable the channel
            timerHandle->CCER &= ~(TIM_CCER_CC1E_Msk);

            //Clear the Output Compare mode
            timerHandle->CCMR1 &= ~(TIM_CCMR1_OC1M_Msk);
            //Set the Output Compare mode
            timerHandle->CCMR1 |= (whichMode << TIM_CCMR1_OC1M_Pos);

            //Disable the preload register
            timerHandle->CCMR1 &= ~(TIM_CCMR1_OC1PE_Msk);

            //Clear the polarity setting
            timerHandle->CCER &= ~(TIM_CCER_CC1P_Msk);
            //Set the polarity
            timerHandle->CCER |= (whichPolarity << TIM_CCER_CC1P_Pos);

            //Set the value
            timerHandle->CCR1 = triggerValue;

            //Enable the Output Compare channel
            timerHandle->CCER |= TIM_CCER_CC1E;
            break;
        case TIM_CHANNEL_2:
        	//Disable the channel
			timerHandle->CCER &= ~(TIM_CCER_CC2E_Msk);

			//Clear the Output Compare mode
			timerHandle->CCMR1 &= ~(TIM_CCMR1_OC2M_Msk);
			//Set the Output Compare mode
			timerHandle->CCMR1 |= (whichMode << TIM_CCMR1_OC2M_Pos);

			//Disable the preload register
			timerHandle->CCMR1 &= ~(TIM_CCMR1_OC2PE_Msk);

			//Clear the polarity setting
			timerHandle->CCER &= ~(TIM_CCER_CC2P_Msk);
			//Set the polarity
			timerHandle->CCER |= (whichPolarity << TIM_CCER_CC2P_Pos);

			//Set the value
			timerHandle->CCR2 = triggerValue;

			//Enable the Output Compare channel
			timerHandle->CCER |= TIM_CCER_CC2E;
            break;
        case TIM_CHANNEL_3:

            break;
        case TIM_CHANNEL_4:

            break;
        default:
            break;
    }
}

void STM_TIM_ConfigureTimerChannelOutputPWM(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel, TIM_CHANNEL_OUTPUT_POLARITIES_t whichPolarity) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }

    timerHandle->CR1 |= TIM_CR1_ARPE;
    timerHandle->BDTR |= TIM_BDTR_MOE;

    switch(whichChannel) {
        case TIM_CHANNEL_1:
            //Disable the channel
            timerHandle->CCER &= ~(TIM_CCER_CC1E_Msk);

            //Clear the Output Compare mode
            timerHandle->CCMR1 &= ~(TIM_CCMR1_OC1M_Msk);
            //Set the Output Compare mode
            if(whichPolarity == TIM_CHANNEL_OUTPUT_POLARITY_ACTIVE_HIGH) {
            	timerHandle->CCMR1 |= (6 << TIM_CCMR1_OC1M_Pos);
            } else {
            	timerHandle->CCMR1 |= (7 << TIM_CCMR1_OC1M_Pos);
            }

            //Disable the preload register
            timerHandle->CCMR1 &= ~(TIM_CCMR1_OC1PE_Msk);
            timerHandle->CCMR1 |= TIM_CCMR1_OC1PE;

            //Clear the polarity setting
            timerHandle->CCER &= ~(TIM_CCER_CC1P_Msk);
            //Set the polarity
            timerHandle->CCER |= (whichPolarity << TIM_CCER_CC1P_Pos);

            //Enable the Output Compare channel
            timerHandle->CCER |= TIM_CCER_CC1E;
            break;
        case TIM_CHANNEL_2:
        	//Disable the channel
			timerHandle->CCER &= ~(TIM_CCER_CC2E_Msk);

			//Clear the Output Compare mode
			timerHandle->CCMR1 &= ~(TIM_CCMR1_OC2M_Msk);
			//Set the Output Compare mode
			if(whichPolarity == TIM_CHANNEL_OUTPUT_POLARITY_ACTIVE_HIGH) {
				timerHandle->CCMR1 |= (6 << TIM_CCMR1_OC2M_Pos);
			} else {
				timerHandle->CCMR1 |= (7 << TIM_CCMR1_OC2M_Pos);
			}

			//Disable the preload register
			timerHandle->CCMR1 &= ~(TIM_CCMR1_OC2PE_Msk);
			timerHandle->CCMR1 |= TIM_CCMR1_OC2PE;

			//Clear the polarity setting
			timerHandle->CCER &= ~(TIM_CCER_CC2P_Msk);
			//Set the polarity
			timerHandle->CCER |= (whichPolarity << TIM_CCER_CC2P_Pos);

			//Enable the Output Compare channel
			timerHandle->CCER |= TIM_CCER_CC2E;
            break;
        case TIM_CHANNEL_3:

            break;
        case TIM_CHANNEL_4:

            break;
        default:
            break;
    }
}

void STM_TIM_DisableTimerChannel(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }


}

void STM_TIM_TimerChannelOutputPWM_UpdatePWMValue(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel, uint8_t pwmValue, uint8_t pwmMax) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }

    timerHandle->ARR = pwmMax;
    switch(whichChannel) {
    case TIM_CHANNEL_1:
    	timerHandle->CCR1 = pwmValue;
    	break;
    case TIM_CHANNEL_2:
    	timerHandle->CCR2 = pwmValue;
    	break;
    case TIM_CHANNEL_3:
		timerHandle->CCR3 = pwmValue;
		break;
	case TIM_CHANNEL_4:
		timerHandle->CCR4 = pwmValue;
		break;
    }

    timerHandle->EGR |= TIM_EGR_UG;
}

void STM_TIM_TimerChannelOutputPWM_Update8BitPWMValue(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel, uint8_t pwmValue) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }


}

void STM_TIM_EnableTimerReloadInterrupt(TIM_MODULES_t whichModule) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandle(whichModule);
    if(timerHandle == 0) {
        //Timer module doesn't exist on this hardware
        return;
    }

    
}

void STM_TIM_DisableTimerReloadInterrupt(TIM_MODULES_t whichModule) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandle(whichModule);
    if(timerHandle == 0) {
        //Timer module doesn't exist on this hardware
        return;
    }


}

void STM_TIM_EnableTimerChannelInterrupt(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return;
    }


}

void STM_TIM_DisableTimerChannelInterrupt(TIM_MODULES_t whichModule, TIM_CHANNELS_t whichChannel) {
    //Try to get the handle for this module
    TIM_TypeDef* timerHandle = STM_TIM_GetTimerHandleIfChannelPresent(whichModule, whichChannel);
    if(timerHandle == 0) {
        //Timer module or channel doesn't exist on this hardware
        return ;
    }


}
