#ifndef __STM32F0_RCC_CONF_H__
#define __STM32F0_RCC_CONF_H__

#include <stdint.h>
#include <stdbool.h>

typedef enum {
    CORECLK_SRC_SYSCLK = 0,
    CORECLK_SRC_HCLK,
    CORECLK_SRC_PCLK1,
} CORECLK_SRC_t;

typedef enum {
    SYSCLK_SRC_HSI = 0,
    SYSCLK_SRC_HSE,
    SYSCLK_SRC_PLL,
} SYSCLK_SRC_t;

typedef enum {
    AHB_SRC_SYSCLK_DIV1 = 0,
    AHB_SRC_SYSCLK_DIV2,
    AHB_SRC_SYSCLK_DIV4,
    AHB_SRC_SYSCLK_DIV8,
    AHB_SRC_SYSCLK_DIV16,
    AHB_SRC_SYSCLK_DIV64,
    AHB_SRC_SYSCLK_DIV128,
    AHB_SRC_SYSCLK_DIV256,
    AHB_SRC_SYSCLK_DIV512,
} AHB_SRC_t;

typedef enum {
    APB1_SRC_HCLK_DIV1 = 0,
    APB1_SRC_HCLK_DIV2,
    APB1_SRC_HCLK_DIV4,
    APB1_SRC_HCLK_DIV8,
    APB1_SRC_HCLK_DIV16,
} APB1_SRC_t;

typedef enum {
    RTC_SRC_OFF = 0,
    RTC_SRC_LSE,
    RTC_SRC_LSI,
    RTC_SRC_HSE_DIV32,
} RTC_SRC_t;

typedef enum {
    PLL_MUL_2 = 0,
    PLL_MUL_3,
    PLL_MUL_4,
    PLL_MUL_5,
    PLL_MUL_6,
    PLL_MUL_7,
    PLL_MUL_8,
    PLL_MUL_9,
    PLL_MUL_10,
    PLL_MUL_11,
    PLL_MUL_12,
    PLL_MUL_13,
    PLL_MUL_14,
    PLL_MUL_15,
    PLL_MUL_16,
} PLL_MUL_t;

typedef enum {
    PLL_PREDIV_DIV1 = 0,
    PLL_PREDIV_DIV2,
    PLL_PREDIV_DIV3,
    PLL_PREDIV_DIV4,
    PLL_PREDIV_DIV5,
    PLL_PREDIV_DIV6,
    PLL_PREDIV_DIV7,
    PLL_PREDIV_DIV8,
    PLL_PREDIV_DIV9,
    PLL_PREDIV_DIV10,
    PLL_PREDIV_DIV11,
    PLL_PREDIV_DIV12,
    PLL_PREDIV_DIV13,
    PLL_PREDIV_DIV14,
    PLL_PREDIV_DIV15,
    PLL_PREDIV_DIV16,
} PLL_PREDIV_t;

typedef enum {
    PLL_CLK_SRC_HSE = 0,
    PLL_CLK_SRC_HSI,
} PLL_CLK_SRC_t;

typedef enum {
    USART1_CLK_SRC_PCLK1 = 0,
    USART1_CLK_SRC_SYSCLK,
    USART1_CLK_SRC_LSE,
    USART1_CLK_SRC_HSI,
} USART1_CLK_SRC_t;

typedef enum {
    I2C1_CLK_SRC_HSI = 0,
    I2C1_CLK_SRC_SYSCLK,
} I2C1_CLK_SRC_t;

typedef enum {
    MCO1_SRC_OFF = 0,
    MCO1_SRC_LSI,
    MCO1_SRC_LSE,
    MCO1_SRC_SYSCLK,
    MCO1_SRC_HSI,
    MCO1_SRC_HSE,
    MCO1_SRC_PLLCLK_DIV2,
    MCO1_SRC_HSI14,
} MCO1_SRC_t;

extern const bool RCC_HSE_ENABLE;
extern const bool RCC_HSE_BYPASS;
extern const uint32_t RCC_HSE_FREQUENCY_HZ;

extern const bool RCC_LSE_ENABLE;
extern const bool RCC_LSE_BYPASS;

extern const bool RCC_HSI_ENABLE;

extern const bool RCC_LSI_ENABLE;

//extern const bool RCC_HSI14_ENABLE;   //TODO: FIXME: Put in ADC library
//extern const bool RCC_HSI48_ENABLE;   //TODO: FIXME: Not supported in the F030 series, might be needed later

extern const bool RCC_PLL_ENABLE;
extern const PLL_CLK_SRC_t RCC_PLL_CLK_SRC;
extern const PLL_PREDIV_t RCC_PLL_DIVIDER;
extern const PLL_MUL_t RCC_PLL_MULTIPLIER;

extern const SYSCLK_SRC_t RCC_SYSCLK_SRC;

//AHB and APB1 dividers are left at 1, for simplicity.

#endif
