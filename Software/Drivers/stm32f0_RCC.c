#include "stm32f0_RCC.h"

#pragma GCC push_options
#pragma GCC optimize 0
void static STM_RCC_BusyWait(void) {
    uint32_t temp = 0;
    while(temp < 10) {
        temp++;
    }
}
void STM_RCC_EnableHSE(void) {
    RCC->CR |= (RCC_CR_HSEON | RCC_CR_CSSON);
    while((RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY) {
        //Do nothing
    }
}
void STM_RCC_EnableLSE(void) {
    RCC->BDCR |= RCC_BDCR_LSEON;
    while((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) {
        //Do nothing
    }
}
void STM_RCC_EnableHSI(void) {
    RCC->CR |= RCC_CR_HSION;
    while((RCC->CR & RCC_CR_HSIRDY) != RCC_CR_HSIRDY) {
        //Do nothing
    }
}
void STM_RCC_EnableLSI(void) {
    RCC->CSR |= RCC_CSR_LSION;
    while((RCC->CSR & RCC_CSR_LSIRDY) != RCC_CSR_LSIRDY) {
        //Do nothing
    }
}
void STM_RCC_EnablePLL(void) {
    RCC->CR |= RCC_CR_PLLON;
    while((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY) {
        //Do nothing
    }
}
#pragma GCC pop_options


void STM_RCC_FlashWaitStates(uint8_t waitStates) {
    if(waitStates < 8) {
        FLASH->ACR = ((FLASH->ACR & ~(FLASH_ACR_LATENCY)) | waitStates);
    }
}

uint8_t STM_RCC_GetFlashWaitStates(void) {
    return (FLASH->ACR & FLASH_ACR_LATENCY);
}

void STM_RCC_SetCoreClock(SYSCLK_SRC_t clockSource) {
    RCC->CFGR = ((RCC->CFGR & ~(RCC_CFGR_SW)) | clockSource);
}

void STM_RCC_ClkInit(void) {
    //Need to calculate what our final core clock will be, so we can set the flash latency correctly, and also abort if it will result in a too-fast clock
    const uint32_t HSI_FREQ_HZ = 8000000;
    uint32_t finalFrequency = 0;
    if(RCC_SYSCLK_SRC == SYSCLK_SRC_HSI) {
        finalFrequency = HSI_FREQ_HZ;
    } else if(RCC_SYSCLK_SRC == SYSCLK_SRC_HSE) {
        finalFrequency = RCC_HSE_FREQUENCY_HZ;
    } else if(RCC_SYSCLK_SRC == SYSCLK_SRC_PLL) {
        uint32_t pllTempFrequency = 0;
        uint32_t pllDivider = 1;
        uint32_t pllMultiplier = 1;
        if(RCC_PLL_CLK_SRC == PLL_CLK_SRC_HSI) {
            #if defined(STM32F030xC)
                pllTempFrequency = HSI_FREQ_HZ;
                pllDivider = (RCC_PLL_DIVIDER+1);
            #else   /* Not an STM32F030xC */
                pllTempFrequency = HSI_FREQ_HZ;
                pllDivider = 2;
            #endif
        } else if(RCC_PLL_CLK_SRC == PLL_CLK_SRC_HSE) {
            pllTempFrequency = RCC_HSE_FREQUENCY_HZ;
            pllDivider = (RCC_PLL_DIVIDER+1);
        }

        pllMultiplier = (RCC_PLL_MULTIPLIER+2);

        finalFrequency = ((pllTempFrequency / pllDivider) * pllMultiplier);
    }
    //Check for a too-fast clock
    if(finalFrequency > 48000000) {
        //Invalid, abort!
        return;
    }

    //Switch to the HSI while we reconfigure other clocks
        //Turn on the HSI if it's not already on
        STM_RCC_EnableHSI();
        //Set our core clock to HSI
        STM_RCC_SetCoreClock(SYSCLK_SRC_HSI);
        STM_RCC_FlashWaitStates(0);

    //Turn things on and configure them
    if(RCC_HSE_ENABLE) {
        //Turn on HSE
        if(RCC_HSE_BYPASS) {
            RCC->CR |= RCC_CR_HSEBYP;
        }
        STM_RCC_EnableHSE();
    }
    if(RCC_LSE_ENABLE) {
        //Turn on LSE
        if(RCC_LSE_BYPASS) {
            RCC->BDCR |= RCC_BDCR_LSEBYP;
        }
        STM_RCC_EnableLSE();
    }
    /*if(RCC_HSI14_ENABLE) {
        //Turn on HSI14
        //TODO: FIXME: Put this in the ADC library
    }*/
    if(RCC_LSI_ENABLE) {
        //Turn on LSI
        STM_RCC_EnableLSI();
    }
    /*
    if(RCC_HSI48_ENABLE) {
        //Turn on HSI48
        //TODO: FIXME: Not supported on the F030 series, but might be needed later
    }
    */
    if(RCC_PLL_ENABLE) {
        //Turn off PLL
        RCC->CR &= ~(RCC_CR_PLLON);

        //Set the PLL source
        if(RCC_PLL_CLK_SRC == PLL_CLK_SRC_HSE) {
            //Set source to HSE
            RCC->CFGR |= RCC_CFGR_PLLSRC;
        } else if(RCC_PLL_CLK_SRC == PLL_CLK_SRC_HSI) {
            //Set source to HSI
            RCC->CFGR &= ~(RCC_CFGR_PLLSRC);
        }
        
        //Set the divider
        // On STM32F030x4, STM32F030x6, STM32F030x8, the predivider is only adjustable for the HSE,
        //  and HSI has a fixed divisor of 2.
        // On STM32F030xC, the predivider affects both HSI and HSE, and HSI has no fixed divisor.
        RCC->CFGR2 = RCC_PLL_DIVIDER;

        //Set the multiplier
        RCC->CFGR &= ~(RCC_CFGR_PLLMUL_Msk);
        RCC->CFGR |= (RCC_PLL_MULTIPLIER << RCC_CFGR_PLLMUL_Pos);

        //And turn on the PLL
        STM_RCC_EnablePLL();
    }    

    //Set the flash wait states higher if needed
    if( (finalFrequency >= 24000000) && (STM_RCC_GetFlashWaitStates() < 1) ) {
        STM_RCC_FlashWaitStates(1);
    }
    //Switch the clock source
    STM_RCC_SetCoreClock(RCC_SYSCLK_SRC);
    //Set the flash wait states lower if needed
    if( (finalFrequency < 24000000) && (STM_RCC_GetFlashWaitStates() > 0)) {
        STM_RCC_FlashWaitStates(0);
    }

    //Go update the SystemCoreClock variable
    SystemCoreClockUpdate();

    //Disable things
    if(!RCC_PLL_ENABLE) {
        //Turn off PLL
        RCC->CR &= ~(RCC_CR_PLLON);
    }
    if(!RCC_HSE_ENABLE) {
        //Turn off HSE
        RCC->CR &= ~(RCC_CR_HSEON);
        RCC->CR &= ~(RCC_CR_HSEBYP);
    }
    if(!RCC_LSE_ENABLE) {
        //Turn off LSE
        RCC->BDCR &= ~(RCC_BDCR_LSEON);
        RCC->BDCR &= ~(RCC_BDCR_LSEBYP);
    }
    if(!RCC_HSI_ENABLE) {
        //Turn off HSI
        RCC->CR &= ~(RCC_CR_HSION);
    }
    if(!RCC_LSI_ENABLE) {
        //Turn off LSI
        RCC->CSR &= ~(RCC_CSR_LSION);
    }
}

void STM_RCC_Peripheral_Reset(RCC_PERIPHERALS_t whichPeripheral) {
    switch(whichPeripheral) {
        case RCC_PERIPHERAL_GPIOA:
            RCC->AHBRSTR |= (RCC_AHBRSTR_GPIOARST);
            break;
        case RCC_PERIPHERAL_GPIOB:
            RCC->AHBRSTR |= (RCC_AHBRSTR_GPIOBRST);
            break;
        case RCC_PERIPHERAL_GPIOC:
            RCC->AHBRSTR |= (RCC_AHBRSTR_GPIOCRST);
            break;
        case RCC_PERIPHERAL_GPIOF:
            RCC->AHBRSTR |= (RCC_AHBRSTR_GPIOFRST);
            break;
        case RCC_PERIPHERAL_TIM3:
            RCC->APB1RSTR |= (RCC_APB1RSTR_TIM3RST);
            break;
        case RCC_PERIPHERAL_TIM14:
            RCC->APB1RSTR |= (RCC_APB1RSTR_TIM14RST);
            break;
        case RCC_PERIPHERAL_WWDG:
            RCC->APB1RSTR |= (RCC_APB1RSTR_WWDGRST);
            break;
        case RCC_PERIPHERAL_I2C1:
            RCC->APB1RSTR |= (RCC_APB1RSTR_I2C1RST);
            break;
        case RCC_PERIPHERAL_PWR:
            RCC->APB1RSTR |= (RCC_APB1RSTR_PWRRST);
            break;
        case RCC_PERIPHERAL_SYSCFG:
            RCC->APB2RSTR |= (RCC_APB2RSTR_SYSCFGRST);
            break;
        case RCC_PERIPHERAL_ADC1:
            RCC->APB2RSTR |= (RCC_APB2RSTR_ADC1RST);
            break;
        case RCC_PERIPHERAL_TIM1:
            RCC->APB2RSTR |= (RCC_APB2RSTR_TIM1RST);
            break;
        case RCC_PERIPHERAL_SPI1:
            RCC->APB2RSTR |= (RCC_APB2RSTR_SPI1RST);
            break;
        case RCC_PERIPHERAL_TIM16:
            RCC->APB2RSTR |= (RCC_APB2RSTR_TIM16RST);
            break;
        case RCC_PERIPHERAL_TIM17:
            RCC->APB2RSTR |= (RCC_APB2RSTR_TIM17RST);
            break;
        case RCC_PERIPHERAL_USART1:
            RCC->APB2RSTR |= (RCC_APB2RSTR_USART1RST);
            break;
        case RCC_PERIPHERAL_DBGMCU:
            RCC->APB2RSTR |= (RCC_APB2RSTR_DBGMCURST);
            break;
    }

    STM_RCC_BusyWait();

    switch(whichPeripheral) {
        case RCC_PERIPHERAL_GPIOA:
            RCC->AHBRSTR &= ~(RCC_AHBRSTR_GPIOARST);
            break;
        case RCC_PERIPHERAL_GPIOB:
            RCC->AHBRSTR &= ~(RCC_AHBRSTR_GPIOBRST);
            break;
        case RCC_PERIPHERAL_GPIOC:
            RCC->AHBRSTR &= ~(RCC_AHBRSTR_GPIOCRST);
            break;
        case RCC_PERIPHERAL_GPIOF:
            RCC->AHBRSTR &= ~(RCC_AHBRSTR_GPIOFRST);
            break;
        case RCC_PERIPHERAL_TIM3:
            RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM3RST);
            break;
        case RCC_PERIPHERAL_TIM14:
            RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM14RST);
            break;
        case RCC_PERIPHERAL_WWDG:
            RCC->APB1RSTR &= ~(RCC_APB1RSTR_WWDGRST);
            break;
        case RCC_PERIPHERAL_I2C1:
            RCC->APB1RSTR &= ~(RCC_APB1RSTR_I2C1RST);
            break;
        case RCC_PERIPHERAL_PWR:
            RCC->APB1RSTR &= ~(RCC_APB1RSTR_PWRRST);
            break;
        case RCC_PERIPHERAL_SYSCFG:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_SYSCFGRST);
            break;
        case RCC_PERIPHERAL_ADC1:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_ADC1RST);
            break;
        case RCC_PERIPHERAL_TIM1:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_TIM1RST);
            break;
        case RCC_PERIPHERAL_SPI1:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_SPI1RST);
            break;
        case RCC_PERIPHERAL_TIM16:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_TIM16RST);
            break;
        case RCC_PERIPHERAL_TIM17:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_TIM17RST);
            break;
        case RCC_PERIPHERAL_USART1:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_USART1RST);
            break;
        case RCC_PERIPHERAL_DBGMCU:
            RCC->APB2RSTR &= ~(RCC_APB2RSTR_DBGMCURST);
            break;
    }
}

void STM_RCC_Peripheral_EnableClock(RCC_PERIPHERALS_t whichPeripheral) {
    switch(whichPeripheral) {
        case RCC_PERIPHERAL_GPIOA:
            RCC->AHBENR |= (RCC_AHBENR_GPIOAEN);
            break;
        case RCC_PERIPHERAL_GPIOB:
            RCC->AHBENR |= (RCC_AHBENR_GPIOBEN);
            break;
        case RCC_PERIPHERAL_GPIOC:
            RCC->AHBENR |= (RCC_AHBENR_GPIOCEN);
            break;
        case RCC_PERIPHERAL_GPIOF:
            RCC->AHBENR |= (RCC_AHBENR_GPIOFEN);
            break;
        case RCC_PERIPHERAL_CRC:
            RCC->AHBENR |= (RCC_AHBENR_CRCEN);
            break;
        case RCC_PERIPHERAL_DMA1:
            RCC->AHBENR |= (RCC_AHBENR_DMA1EN);
            break;
        case RCC_PERIPHERAL_SRAM:
            RCC->AHBENR |= (RCC_AHBENR_SRAMEN);
            break;
        case RCC_PERIPHERAL_FLITF:
            RCC->AHBENR |= (RCC_AHBENR_FLITFEN);
            break;

        case RCC_PERIPHERAL_TIM3:
            RCC->APB1ENR |= (RCC_APB1ENR_TIM3EN);
            break;
        case RCC_PERIPHERAL_TIM14:
            RCC->APB1ENR |= (RCC_APB1ENR_TIM14EN);
            break;
        case RCC_PERIPHERAL_WWDG:
            RCC->APB1ENR |= (RCC_APB1ENR_WWDGEN);
            break;
        case RCC_PERIPHERAL_I2C1:
            RCC->APB1ENR |= (RCC_APB1ENR_I2C1EN);
            break;
        case RCC_PERIPHERAL_PWR:
            RCC->APB1ENR |= (RCC_APB1ENR_PWREN);
            break;
        
        case RCC_PERIPHERAL_SYSCFG:
            RCC->APB2ENR |= (RCC_APB2ENR_SYSCFGEN);
            break;
        case RCC_PERIPHERAL_ADC1:
            RCC->APB2ENR |= (RCC_APB2ENR_ADC1EN);
            break;
        case RCC_PERIPHERAL_TIM1:
            RCC->APB2ENR |= (RCC_APB2ENR_TIM1EN);
            break;
        case RCC_PERIPHERAL_SPI1:
            RCC->APB2ENR |= (RCC_APB2ENR_SPI1EN);
            break;
        case RCC_PERIPHERAL_TIM16:
            RCC->APB2ENR |= (RCC_APB2ENR_TIM16EN);
            break;
        case RCC_PERIPHERAL_TIM17:
            RCC->APB2ENR |= (RCC_APB2ENR_TIM17EN);
            break;
        case RCC_PERIPHERAL_USART1:
            RCC->APB2ENR |= (RCC_APB2ENR_USART1EN);
            break;
        case RCC_PERIPHERAL_DBGMCU:
            RCC->APB2ENR |= (RCC_APB2ENR_DBGMCUEN);
            break;
    }

    STM_RCC_BusyWait();
}

void STM_RCC_Peripheral_DisableClock(RCC_PERIPHERALS_t whichPeripheral) {
    switch(whichPeripheral) {
        case RCC_PERIPHERAL_GPIOA:
            RCC->AHBENR &= ~(RCC_AHBENR_GPIOAEN);
            break;
        case RCC_PERIPHERAL_GPIOB:
            RCC->AHBENR &= ~(RCC_AHBENR_GPIOBEN);
            break;
        case RCC_PERIPHERAL_GPIOC:
            RCC->AHBENR &= ~(RCC_AHBENR_GPIOCEN);
            break;
        case RCC_PERIPHERAL_GPIOF:
            RCC->AHBENR &= ~(RCC_AHBENR_GPIOFEN);
            break;
        case RCC_PERIPHERAL_CRC:
            RCC->AHBENR &= ~(RCC_AHBENR_CRCEN);
            break;
        case RCC_PERIPHERAL_DMA1:
            RCC->AHBENR &= ~(RCC_AHBENR_DMA1EN);
            break;
        case RCC_PERIPHERAL_SRAM:
            RCC->AHBENR &= ~(RCC_AHBENR_SRAMEN);
            break;
        case RCC_PERIPHERAL_FLITF:
            RCC->AHBENR &= ~(RCC_AHBENR_FLITFEN);
            break;

        case RCC_PERIPHERAL_TIM3:
            RCC->APB1ENR &= ~(RCC_APB1ENR_TIM3EN);
            break;
        case RCC_PERIPHERAL_TIM14:
            RCC->APB1ENR &= ~(RCC_APB1ENR_TIM14EN);
            break;
        case RCC_PERIPHERAL_WWDG:
            RCC->APB1ENR &= ~(RCC_APB1ENR_WWDGEN);
            break;
        case RCC_PERIPHERAL_I2C1:
            RCC->APB1ENR &= ~(RCC_APB1ENR_I2C1EN);
            break;
        case RCC_PERIPHERAL_PWR:
            RCC->APB1ENR &= ~(RCC_APB1ENR_PWREN);
            break;
        
        case RCC_PERIPHERAL_SYSCFG:
            RCC->APB2ENR &= ~(RCC_APB2ENR_SYSCFGEN);
            break;
        case RCC_PERIPHERAL_ADC1:
            RCC->APB2ENR &= ~(RCC_APB2ENR_ADC1EN);
            break;
        case RCC_PERIPHERAL_TIM1:
            RCC->APB2ENR &= ~(RCC_APB2ENR_TIM1EN);
            break;
        case RCC_PERIPHERAL_SPI1:
            RCC->APB2ENR &= ~(RCC_APB2ENR_SPI1EN);
            break;
        case RCC_PERIPHERAL_TIM16:
            RCC->APB2ENR &= ~(RCC_APB2ENR_TIM16EN);
            break;
        case RCC_PERIPHERAL_TIM17:
            RCC->APB2ENR &= ~(RCC_APB2ENR_TIM17EN);
            break;
        case RCC_PERIPHERAL_USART1:
            RCC->APB2ENR &= ~(RCC_APB2ENR_USART1EN);
            break;
        case RCC_PERIPHERAL_DBGMCU:
            RCC->APB2ENR &= ~(RCC_APB2ENR_DBGMCUEN);
            break;
    }

    STM_RCC_BusyWait();
}
