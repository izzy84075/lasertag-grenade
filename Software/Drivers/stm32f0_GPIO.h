#ifndef __STM32F0_GPIO_H__
#define __STM32F0_GPIO_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include "stm32f0_RCC.h"

typedef enum {
    GPIO_PORTA = 0,
    GPIO_PORTB,
    GPIO_PORTC,
    GPIO_PORTD,
    GPIO_PORTE,
    GPIO_PORTF,
    GPIO_PORTG,
    GPIO_PORTH,
    GPIO_PORTI,
    GPIO_PORTJ,
    GPIO_PORTK,
} GPIO_PORTS_t;

typedef enum {
    GPIO_PIN0 = 0x0001,
    GPIO_PIN1 = 0x0002,
    GPIO_PIN2 = 0x0004,
    GPIO_PIN3 = 0x0008,
    GPIO_PIN4 = 0x0010,
    GPIO_PIN5 = 0x0020,
    GPIO_PIN6 = 0x0040,
    GPIO_PIN7 = 0x0080,
    GPIO_PIN8 = 0x0100,
    GPIO_PIN9 = 0x0200,
    GPIO_PIN10 = 0x0400,
    GPIO_PIN11 = 0x0800,
    GPIO_PIN12 = 0x1000,
    GPIO_PIN13 = 0x2000,
    GPIO_PIN14 = 0x4000,
    GPIO_PIN15 = 0x8000,
} GPIO_PINS_t;

typedef enum {
    GPIO_MODE_INPUT = 0,
    GPIO_MODE_OUTPUT,
    GPIO_MODE_ALTERNATE_FUNCTION,
    GPIO_MODE_ANALOG,
} GPIO_MODES_t;

typedef enum {
    GPIO_OUTPUT_TYPE_PUSHPULL = 0,
    GPIO_OUTPUT_TYPE_OPENDRAIN,
} GPIO_OUTPUT_TYPES_t;

typedef enum {
    GPIO_NO_PULL = 0,
    GPIO_PULL_UP,
    GPIO_PULL_DOWN,
} GPIO_PULL_MODES_t;

typedef enum {
    GPIO_OUTPUT_SPEED_2MHZ = 0,
    GPIO_OUTPUT_SPEED_10MHZ,
    GPIO_OUTPUT_SPEED_RESERVED,
    GPIO_OUTPUT_SPEED_50MHZ,
} GPIO_OUTPUT_SPEEDS_t;

void STM_GPIO_EnablePort(GPIO_PORTS_t whichPort);
void STM_GPIO_DisablePort(GPIO_PORTS_t whichPort);

void STM_GPIO_SetPinState(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, bool whichState);

void STM_GPIO_SetPinMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_MODES_t whichMode);
void STM_GPIO_SetPinAlternateFunction(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, uint8_t whichAlternateFunction);
void STM_GPIO_SetPinOutputMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_OUTPUT_TYPES_t outputMode);
void STM_GPIO_SetPinPullMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_PULL_MODES_t pullMode);
void STM_GPIO_SetPinOutputSpeed(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_OUTPUT_SPEEDS_t whichSpeed);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
