#include "stm32f0_RCC_Conf.h"

const bool RCC_HSE_ENABLE = false;
const bool RCC_HSE_BYPASS = false;
const uint32_t RCC_HSE_FREQUENCY_HZ = 8000000;

const bool RCC_LSE_ENABLE = false;
const bool RCC_LSE_BYPASS = false;

const bool RCC_HSI_ENABLE = true;

const bool RCC_LSI_ENABLE = true;

const bool RCC_PLL_ENABLE = true;
const PLL_CLK_SRC_t RCC_PLL_CLK_SRC = PLL_CLK_SRC_HSI;
const PLL_PREDIV_t RCC_PLL_DIVIDER = PLL_PREDIV_DIV2;
const PLL_MUL_t RCC_PLL_MULTIPLIER = PLL_MUL_12;

const SYSCLK_SRC_t RCC_SYSCLK_SRC = SYSCLK_SRC_PLL;
