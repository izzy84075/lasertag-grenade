#include "stm32f0_GPIO.h"

void STM_GPIO_EnablePort(GPIO_PORTS_t whichPort) {
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOA);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOB);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOC);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOD);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOE);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOF);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOG);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOH);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOI);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOJ);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            STM_RCC_Peripheral_EnableClock(RCC_PERIPHERAL_GPIOK);
            break;
        #endif
    }
}

void STM_GPIO_DisablePort(GPIO_PORTS_t whichPort) {
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOA);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOB);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOC);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOD);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOE);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOF);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOG);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOH);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOI);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOJ);
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            STM_RCC_Peripheral_DisableClock(RCC_PERIPHERAL_GPIOK);
            break;
        #endif
    }
}

void STM_GPIO_SetPinState(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, bool whichState) {
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            if(whichState) {
                GPIOA->BSRR = whichPins;
            } else {
                GPIOA->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            if(whichState) {
                GPIOB->BSRR = whichPins;
            } else {
                GPIOB->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            if(whichState) {
                GPIOC->BSRR = whichPins;
            } else {
                GPIOC->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            if(whichState) {
                GPIOD->BSRR = whichPins;
            } else {
                GPIOD->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            if(whichState) {
                GPIOE->BSRR = whichPins;
            } else {
                GPIOE->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            if(whichState) {
                GPIOF->BSRR = whichPins;
            } else {
                GPIOF->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            if(whichState) {
                GPIOG->BSRR = whichPins;
            } else {
                GPIOG->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            if(whichState) {
                GPIOH->BSRR = whichPins;
            } else {
                GPIOH->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            if(whichState) {
                GPIOI->BSRR = whichPins;
            } else {
                GPIOI->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            if(whichState) {
                GPIOJ->BSRR = whichPins;
            } else {
                GPIOJ->BRR = whichPins;
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            if(whichState) {
                GPIOK->BSRR = whichPins;
            } else {
                GPIOK->BRR = whichPins;
            }
            break;
        #endif
    }
}



void STM_GPIO_SetPinMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_MODES_t whichMode) {
    uint8_t i;
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOA->MODER = ( (GPIOA->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOB->MODER = ( (GPIOB->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOC->MODER = ( (GPIOC->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOD->MODER = ( (GPIOD->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOE->MODER = ( (GPIOE->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOF->MODER = ( (GPIOF->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOG->MODER = ( (GPIOG->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOH->MODER = ( (GPIOH->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOI->MODER = ( (GPIOI->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOJ->MODER = ( (GPIOJ->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOK->MODER = ( (GPIOK->MODER & ~(0x3 << (i << 1))) | (whichMode << (i << 1)) );
                }
            }
            break;
        #endif
    }
}

void STM_GPIO_SetPinAlternateFunction(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, uint8_t whichAlternateFunction) {
    uint8_t i;
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    if(whichAlternateFunction > 15) {
        //More alternate functions than the STM32F0 supports.
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_GPIOA_HAS_AFR == true
        case GPIO_PORTA:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOA->AFR[0] &= ~(0xF << (i << 2));
                    GPIOA->AFR[0] |= (whichAlternateFunction << (i << 2));
                }
                if(whichPins & (1 << (i+8))) {
                    GPIOA->AFR[1] &= ~(0xF << (i << 2));
					GPIOA->AFR[1] |= (whichAlternateFunction << (i << 2));
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOB_HAS_AFR == true
        case GPIO_PORTB:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOB->AFR[0] = ( (GPIOB->AFR[0] & ~(0xF << (i << 2))) | (whichAlternateFunction << (i << 2)) );
                }
                if(whichPins & (1 << (i+8))) {
                    GPIOB->AFR[1] = ( (GPIOB->AFR[1] & ~(0xF << (i << 2))) | (whichAlternateFunction << (i << 2)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOC_HAS_AFR == true
        case GPIO_PORTC:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOC->AFR[0] = ( (GPIOC->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOC->AFR[1] = ( (GPIOC->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOD_HAS_AFR == true
        case GPIO_PORTD:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOD->AFR[0] = ( (GPIOD->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOD->AFR[1] = ( (GPIOD->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOE_HAS_AFR == true
        case GPIO_PORTE:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOE->AFR[0] = ( (GPIOE->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOE->AFR[1] = ( (GPIOE->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOF_HAS_AFR == true
        case GPIO_PORTF:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOF->AFR[0] = ( (GPIOF->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOF->AFR[1] = ( (GPIOF->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOG_HAS_AFR == true
        case GPIO_PORTG:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOG->AFR[0] = ( (GPIOG->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOG->AFR[1] = ( (GPIOG->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOH_HAS_AFR == true
        case GPIO_PORTH:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOH->AFR[0] = ( (GPIOH->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOH->AFR[1] = ( (GPIOH->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOI_HAS_AFR == true
        case GPIO_PORTI:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOI->AFR[0] = ( (GPIOI->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOI->AFR[1] = ( (GPIOI->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOJ_HAS_AFR == true
        case GPIO_PORTJ:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOJ->AFR[0] = ( (GPIOJ->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOJ->AFR[1] = ( (GPIOJ->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_GPIOK_HAS_AFR == true
        case GPIO_PORTK:
            for(i=0; i<8; i++) {
                if(whichPins & (1 << i)) {
                    GPIOK->AFR[0] = ( (GPIOK->AFR[0] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
                if(whichPins & (1 << (i+8)) {
                    GPIOK->AFR[1] = ( (GPIOK->AFR[1] & ~(0xF << (i << 3))) | (whichAlternateFunction << (i << 3)) );
                }
            }
            break;
        #endif
    }
}

void STM_GPIO_SetPinOutputMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_OUTPUT_TYPES_t outputMode) {
    uint8_t i;
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOA->OTYPER = ( (GPIOA->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOB->OTYPER = ( (GPIOB->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOC->OTYPER = ( (GPIOC->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOD->OTYPER = ( (GPIOD->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOE->OTYPER = ( (GPIOE->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOF->OTYPER = ( (GPIOF->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOG->OTYPER = ( (GPIOG->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOH->OTYPER = ( (GPIOH->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOI->OTYPER = ( (GPIOI->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOJ->OTYPER = ( (GPIOJ->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOK->OTYPER = ( (GPIOK->OTYPER & ~(0x1 << i)) | (outputMode << i) );
                }
            }
            break;
        #endif
    }
}

void STM_GPIO_SetPinPullMode(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_PULL_MODES_t pullMode) {
    uint8_t i;
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOA->PUPDR = ( (GPIOA->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOB->PUPDR = ( (GPIOB->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOC->PUPDR = ( (GPIOC->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOD->PUPDR = ( (GPIOD->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOE->PUPDR = ( (GPIOE->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOF->PUPDR = ( (GPIOF->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOG->PUPDR = ( (GPIOG->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOH->PUPDR = ( (GPIOH->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOI->PUPDR = ( (GPIOI->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOJ->PUPDR = ( (GPIOJ->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOK->PUPDR = ( (GPIOK->PUPDR & ~(0x3 << (i << 1))) | (pullMode << (i << 1)) );
                }
            }
            break;
        #endif
    }
}

void STM_GPIO_SetPinOutputSpeed(GPIO_PORTS_t whichPort, GPIO_PINS_t whichPins, GPIO_OUTPUT_SPEEDS_t whichSpeed) {
    uint8_t i;
    if(whichPins > 0xFFFF) {
        //Too many pins for an STM32 port
        return;
    }
    switch(whichPort) {
        default:
            break;
        #if SELECTED_CHIP_HAS_GPIOA == true
        case GPIO_PORTA:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOA->OSPEEDR = ( (GPIOA->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOB == true
        case GPIO_PORTB:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOB->OSPEEDR = ( (GPIOB->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOC == true
        case GPIO_PORTC:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOC->OSPEEDR = ( (GPIOC->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOD == true
        case GPIO_PORTD:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOD->OSPEEDR = ( (GPIOD->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOE == true
        case GPIO_PORTE:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOE->OSPEEDR = ( (GPIOE->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOF == true
        case GPIO_PORTF:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOF->OSPEEDR = ( (GPIOF->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOG == true
        case GPIO_PORTG:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOG->OSPEEDR = ( (GPIOG->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOH == true
        case GPIO_PORTH:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOH->OSPEEDR = ( (GPIOH->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOI == true
        case GPIO_PORTI:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOI->OSPEEDR = ( (GPIOI->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOJ == true
        case GPIO_PORTJ:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOJ->OSPEEDR = ( (GPIOJ->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
        #if SELECTED_CHIP_HAS_GPIOK == true
        case GPIO_PORTK:
            for(i=0; i<16; i++) {
                if(whichPins & (1 << i)) {
                    GPIOK->OSPEEDR = ( (GPIOK->OSPEEDR & ~(0x3 << (i << 1))) | (whichSpeed << (i << 1)) );
                }
            }
            break;
        #endif
    }
}
