#ifndef __STM32F0_SPI_CONF_H__
#define __STM32F0_SPI_CONF_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

typedef enum {
    SPI_MODE_0 = 0,
    SPI_MODE_1,
    SPI_MODE_2,
    SPI_MODE_3,
} SPI_MODE_t;

typedef enum {
    SPI_CLK_SRC_APB1_DIV2 = 0,
    SPI_CLK_SRC_APB1_DIV4,
    SPI_CLK_SRC_APB1_DIV8,
    SPI_CLK_SRC_APB1_DIV16,
    SPI_CLK_SRC_APB1_DIV32,
    SPI_CLK_SRC_APB1_DIV64,
    SPI_CLK_SRC_APB1_DIV128,
    SPI_CLK_SRC_APB1_DIV256,
} SPI_CLK_SRCS_t;

#if defined(STM32F030x6)
    typedef enum {
        SPI1_MOSI_DISABLED = 0,
        SPI1_MOSI_GPIOA7,
    } SPI1_MOSI_PINS_t;

    typedef enum {
        SPI1_MISO_DISABLED = 0,
        SPI1_MISO_GPIOA6,
    } SPI1_MISO_PINS_t;

    typedef enum {
        SPI1_SCK_DISABLED = 0,
        SPI1_SCK_GPIOA5,
    } SPI1_SCK_PINS_t;
#endif

extern const SPI_CLK_SRCS_t SPI1_CLK_SRC;

extern const SPI_MODE_t SPI1_SPI_MODE;

extern const SPI1_MOSI_PINS_t SPI1_MOSI_PIN;
extern const SPI1_MISO_PINS_t SPI1_MISO_PIN;
extern const SPI1_SCK_PINS_t SPI1_SCK_PIN;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
