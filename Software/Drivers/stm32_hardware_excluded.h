#ifndef __STM32_HARDWARE_EXCLUDED_H__
#define __STM32_HARDWARE_EXCLUDED_H__

#include <stdbool.h>

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

//Check for excluded GPIO ports/features
#if !defined(SELECTED_CHIP_HAS_GPIOA) 
    #define SELECTED_CHIP_HAS_GPIOA         false
#endif
#if !defined(SELECTED_CHIP_GPIOA_HAS_AFR) 
    #define SELECTED_CHIP_GPIOA_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOB) 
    #define SELECTED_CHIP_HAS_GPIOB         false
#endif
#if !defined(SELECTED_CHIP_GPIOB_HAS_AFR) 
    #define SELECTED_CHIP_GPIOB_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOC) 
    #define SELECTED_CHIP_HAS_GPIOC         false
#endif
#if !defined(SELECTED_CHIP_GPIOC_HAS_AFR) 
    #define SELECTED_CHIP_GPIOC_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOD) 
    #define SELECTED_CHIP_HAS_GPIOD         false
#endif
#if !defined(SELECTED_CHIP_GPIOD_HAS_AFR) 
    #define SELECTED_CHIP_GPIOD_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOE) 
    #define SELECTED_CHIP_HAS_GPIOE         false
#endif
#if !defined(SELECTED_CHIP_GPIOE_HAS_AFR) 
    #define SELECTED_CHIP_GPIOE_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOF) 
    #define SELECTED_CHIP_HAS_GPIOF         false
#endif
#if !defined(SELECTED_CHIP_GPIOF_HAS_AFR) 
    #define SELECTED_CHIP_GPIOF_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOG) 
    #define SELECTED_CHIP_HAS_GPIOG         false
#endif
#if !defined(SELECTED_CHIP_GPIOG_HAS_AFR) 
    #define SELECTED_CHIP_GPIOG_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOH) 
    #define SELECTED_CHIP_HAS_GPIOH         false
#endif
#if !defined(SELECTED_CHIP_GPIOH_HAS_AFR) 
    #define SELECTED_CHIP_GPIOH_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOI) 
    #define SELECTED_CHIP_HAS_GPIOI         false
#endif
#if !defined(SELECTED_CHIP_GPIOI_HAS_AFR) 
    #define SELECTED_CHIP_GPIOI_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOJ) 
    #define SELECTED_CHIP_HAS_GPIOJ         false
#endif
#if !defined(SELECTED_CHIP_GPIOJ_HAS_AFR) 
    #define SELECTED_CHIP_GPIOJ_HAS_AFR         false
#endif
#if !defined(SELECTED_CHIP_HAS_GPIOK) 
    #define SELECTED_CHIP_HAS_GPIOK         false
#endif
#if !defined(SELECTED_CHIP_GPIOK_HAS_AFR) 
    #define SELECTED_CHIP_GPIOK_HAS_AFR         false
#endif

//Check for excluded Hardware SEMaphore
#if !defined(SELECTED_CHIP_HAS_HSEM) 
    #define SELECTED_CHIP_HAS_HSEM         false
#endif

//Check for excluded Cyclic Redundancy Check hardware
#if !defined(SELECTED_CHIP_HAS_CRC) 
    #define SELECTED_CHIP_HAS_CRC         false
#endif

//Check for excluded DMA hardware/features
#if !defined(SELECTED_CHIP_HAS_CRC) 
    #define SELECTED_CHIP_HAS_CRC               false
    #define SELECTED_CHIP_NUM_MDMA_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_DMA1) 
    #define SELECTED_CHIP_HAS_DMA1              false
    #define SELECTED_CHIP_NUM_DMA1_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_DMA2) 
    #define SELECTED_CHIP_HAS_DMA2              false
    #define SELECTED_CHIP_NUM_DMA2_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_BASIC_DMA) 
    #define SELECTED_CHIP_HAS_BASIC_DMA              false
    #define SELECTED_CHIP_NUM_BASIC_DMA_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_DMAMUX1) 
    #define SELECTED_CHIP_HAS_DMAMUX1         false
#endif
#if !defined(SELECTED_CHIP_HAS_DMAMUX2) 
    #define SELECTED_CHIP_HAS_DMAMUX2         false
#endif
#if !defined(SELECTED_CHIP_HAS_DMA2D) 
    #define SELECTED_CHIP_HAS_DMA2D         false
#endif

//Check for excluded Flexible Memory Controller
#if !defined(SELECTED_CHIP_HAS_FMC) 
    #define SELECTED_CHIP_HAS_FMC         false
#endif

//Check for excluded DAC hardware
#if !defined(SELECTED_CHIP_HAS_DAC1) 
    #define SELECTED_CHIP_HAS_DAC1         false
#endif
#if !defined(SELECTED_CHIP_HAS_DAC2) 
    #define SELECTED_CHIP_HAS_DAC2         false
#endif

//Check for excluded ADC hardware
#if !defined(SELECTED_CHIP_HAS_ADC1) 
    #define SELECTED_CHIP_HAS_ADC1         false
#endif
#if !defined(SELECTED_CHIP_HAS_ADC2) 
    #define SELECTED_CHIP_HAS_ADC2         false
#endif
#if !defined(SELECTED_CHIP_HAS_ADC3) 
    #define SELECTED_CHIP_HAS_ADC3         false
#endif
#if !defined(SELECTED_CHIP_HAS_ADC4) 
    #define SELECTED_CHIP_HAS_ADC4         false
#endif

//Check for excluded COMParator hardware
#if !defined(SELECTED_CHIP_HAS_COMP1) 
    #define SELECTED_CHIP_HAS_COMP1         false
#endif
#if !defined(SELECTED_CHIP_HAS_COMP2) 
    #define SELECTED_CHIP_HAS_COMP2         false
#endif

//Check for excluded OPAMP hardware
#if !defined(SELECTED_CHIP_HAS_OPAMP1) 
    #define SELECTED_CHIP_HAS_OPAMP1         false
#endif
#if !defined(SELECTED_CHIP_HAS_OPAMP2) 
    #define SELECTED_CHIP_HAS_OPAMP2         false
#endif

//Check for excluded voltage reference hardware
#if !defined(SELECTED_CHIP_HAS_VREFBUF) 
    #define SELECTED_CHIP_HAS_VREFBUF         false
#endif

//Check for excluded Sigma-Delta Modulator Filter hardware
#if !defined(SELECTED_CHIP_HAS_DFSDM) 
    #define SELECTED_CHIP_HAS_DFSDM         false
#endif

//Check for excluded Touch Sensor Controller hardware
#if !defined(SELECTED_CHIP_HAS_TSC) 
    #define SELECTED_CHIP_HAS_TSC         false
#endif

//Check for excluded High-Resolution Timer
#if !defined(SELECTED_CHIP_HAS_HRTIM) 
    #define SELECTED_CHIP_HAS_HRTIM         false
    #define SELECTED_CHIP_NUM_HRTIM_CHANNELS     0
#endif

//Check for excluded Advanced Control Timers (with 16-bit autoreload up/down counter, quadrature input, complementary outputs, and repetition counter)
#if !defined(SELECTED_CHIP_HAS_TIM1) 
    #define SELECTED_CHIP_HAS_TIM1              false
    #define SELECTED_CHIP_NUM_TIM1_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM8) 
    #define SELECTED_CHIP_HAS_TIM8         false
    #define SELECTED_CHIP_NUM_TIM8_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM20) 
    #define SELECTED_CHIP_HAS_TIM20         false
    #define SELECTED_CHIP_NUM_TIM20_CHANNELS     0
#endif

//Check for excluded General-Purpose Timers (with 32-bit autoreload up/down counter and quadrature input)
#if !defined(SELECTED_CHIP_HAS_TIM2) 
    #define SELECTED_CHIP_HAS_TIM2         false
    #define SELECTED_CHIP_NUM_TIM2_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM5) 
    #define SELECTED_CHIP_HAS_TIM5         false
    #define SELECTED_CHIP_NUM_TIM5_CHANNELS     0
#endif

//Check for excluded General-Purpose Timers (With 16-bit autoreload up/down counter and quadrature input)
#if !defined(SELECTED_CHIP_HAS_TIM3) 
    #define SELECTED_CHIP_HAS_TIM3         false
    #define SELECTED_CHIP_NUM_TIM3_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM4) 
    #define SELECTED_CHIP_HAS_TIM4         false
    #define SELECTED_CHIP_NUM_TIM4_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM19) 
    #define SELECTED_CHIP_HAS_TIM19         false
    #define SELECTED_CHIP_NUM_TIM19_CHANNELS     0
#endif

//Check for excluded Basic Timers (Timebase generation)
#if !defined(SELECTED_CHIP_HAS_TIM6) 
    #define SELECTED_CHIP_HAS_TIM6         false
    #define SELECTED_CHIP_NUM_TIM6_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM7) 
    #define SELECTED_CHIP_HAS_TIM7         false
    #define SELECTED_CHIP_NUM_TIM7_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM18) 
    #define SELECTED_CHIP_HAS_TIM18         false
    #define SELECTED_CHIP_NUM_TIM18_CHANNELS     0
#endif

//Check for excluded General-Purpose Timers (With 16-bit autoreload upcounter)
#if !defined(SELECTED_CHIP_HAS_TIM9) 
    #define SELECTED_CHIP_HAS_TIM9 false
    #define SELECTED_CHIP_NUM_TIM9_CHANNELS      0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM10) 
    #define SELECTED_CHIP_HAS_TIM10 false
    #define SELECTED_CHIP_NUM_TIM10_CHANNELS      0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM11) 
    #define SELECTED_CHIP_HAS_TIM11 false
    #define SELECTED_CHIP_NUM_TIM11_CHANNELS      0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM12) 
    #define SELECTED_CHIP_HAS_TIM12         false
    #define SELECTED_CHIP_NUM_TIM12_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM13) 
    #define SELECTED_CHIP_HAS_TIM13         false
    #define SELECTED_CHIP_NUM_TIM13_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM14) 
    #define SELECTED_CHIP_HAS_TIM14         false
    #define SELECTED_CHIP_NUM_TIM14_CHANNELS     0
#endif

//Check for excluded General-Purpose Timers (With 16-bit autoreload upcounter and complementary outputs)
#if !defined(SELECTED_CHIP_HAS_TIM15) 
    #define SELECTED_CHIP_HAS_TIM15         false
    #define SELECTED_CHIP_NUM_TIM15_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM16) 
    #define SELECTED_CHIP_HAS_TIM16         false
    #define SELECTED_CHIP_NUM_TIM16_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_TIM17) 
    #define SELECTED_CHIP_HAS_TIM17         false
    #define SELECTED_CHIP_NUM_TIM17_CHANNELS     0
#endif

//Check for excluded Low-Power Timers (With 16-bit autoreload upcounter, 3-bit prescaler, and quadrature input)
#if !defined(SELECTED_CHIP_HAS_LPTIM1) 
    #define SELECTED_CHIP_HAS_LPTIM1         false
    #define SELECTED_CHIP_NUM_LPTIM1_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_LPTIM2) 
    #define SELECTED_CHIP_HAS_LPTIM2         false
    #define SELECTED_CHIP_NUM_LPTIM2_CHANNELS     0
#endif

//Check for excluded Low-Power Timers (With 16-bit autoreload upcounter and 3-bit prescaler)
#if !defined(SELECTED_CHIP_HAS_LPTIM3) 
    #define SELECTED_CHIP_HAS_LPTIM3         false
    #define SELECTED_CHIP_NUM_LPTIM3_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_LPTIM4) 
    #define SELECTED_CHIP_HAS_LPTIM4         false
    #define SELECTED_CHIP_NUM_LPTIM4_CHANNELS     0
#endif
#if !defined(SELECTED_CHIP_HAS_LPTIM5) 
    #define SELECTED_CHIP_HAS_LPTIM5         false
    #define SELECTED_CHIP_NUM_LPTIM5_CHANNELS     0
#endif

//Check for excluded Real Time Clock hardware
#if !defined(SELECTED_CHIP_HAS_RTC) 
    #define SELECTED_CHIP_HAS_RTC         false
#endif

//Check for excluded Watchdog hardware
#if !defined(SELECTED_CHIP_HAS_IWDG) 
    #define SELECTED_CHIP_HAS_IWDG         false
#endif
#if !defined(SELECTED_CHIP_HAS_WWDG) 
    #define SELECTED_CHIP_HAS_WWDG         false
#endif

//Check for excluded Communication Bus hardware
#if !defined(SELECTED_CHIP_HAS_QUADSPI) 
    #define SELECTED_CHIP_HAS_QUADSPI         false
#endif

#if !defined(SELECTED_CHIP_HAS_SPI1) 
    #define SELECTED_CHIP_HAS_SPI1         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI2) 
    #define SELECTED_CHIP_HAS_SPI2         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI3) 
    #define SELECTED_CHIP_HAS_SPI3         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI4) 
    #define SELECTED_CHIP_HAS_SPI4         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI5) 
    #define SELECTED_CHIP_HAS_SPI5         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI6) 
    #define SELECTED_CHIP_HAS_SPI6         false
#endif

#if !defined(SELECTED_CHIP_HAS_I2C1) 
    #define SELECTED_CHIP_HAS_I2C1         false
#endif
#if !defined(SELECTED_CHIP_HAS_I2C2) 
    #define SELECTED_CHIP_HAS_I2C2         false
#endif
#if !defined(SELECTED_CHIP_HAS_I2C3) 
    #define SELECTED_CHIP_HAS_I2C3         false
#endif
#if !defined(SELECTED_CHIP_HAS_I2C4) 
    #define SELECTED_CHIP_HAS_I2C4         false
#endif

#if !defined(SELECTED_CHIP_HAS_SPI2S1) 
    #define SELECTED_CHIP_HAS_SPI2S1         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI2S2) 
    #define SELECTED_CHIP_HAS_SPI2S2         false
#endif
#if !defined(SELECTED_CHIP_HAS_SPI2S3) 
    #define SELECTED_CHIP_HAS_SPI2S3         false
#endif

#if !defined(SELECTED_CHIP_HAS_USART1) 
    #define SELECTED_CHIP_HAS_USART1         false
#endif
#if !defined(SELECTED_CHIP_HAS_USART2) 
    #define SELECTED_CHIP_HAS_USART2         false
#endif
#if !defined(SELECTED_CHIP_HAS_USART3) 
    #define SELECTED_CHIP_HAS_USART3         false
#endif
#if !defined(SELECTED_CHIP_HAS_USART6) 
    #define SELECTED_CHIP_HAS_USART6         false
#endif

#if !defined(SELECTED_CHIP_HAS_UART4) 
    #define SELECTED_CHIP_HAS_UART4         false
#endif
#if !defined(SELECTED_CHIP_HAS_UART5) 
    #define SELECTED_CHIP_HAS_UART5         false
#endif
#if !defined(SELECTED_CHIP_HAS_UART7) 
    #define SELECTED_CHIP_HAS_UART7         false
#endif
#if !defined(SELECTED_CHIP_HAS_UART8) 
    #define SELECTED_CHIP_HAS_UART8         false
#endif

#if !defined(SELECTED_CHIP_HAS_SWPMI) 
    #define SELECTED_CHIP_HAS_SWPMI         false
#endif

#if !defined(SELECTED_CHIP_HAS_MDIOS) 
    #define SELECTED_CHIP_HAS_MDIOS         false
#endif

#if !defined(SELECTED_CHIP_HAS_SDMCC) 
    #define SELECTED_CHIP_HAS_SDMCC         false
#endif

#if !defined(SELECTED_CHIP_HAS_FDCAN) 
    #define SELECTED_CHIP_HAS_FDCAN         false
#endif

//Check for excluded USB hardware
#if !defined(SELECTED_CHIP_HAS_USB) 
    #define SELECTED_CHIP_HAS_USB         false
#endif
#if !defined(SELECTED_CHIP_HAS_OTG_HS) 
    #define SELECTED_CHIP_HAS_OTG_HS         false
#endif

//Check for excluded Ethernet hardware
#if !defined(SELECTED_CHIP_HAS_ETH) 
    #define SELECTED_CHIP_HAS_ETH         false
#endif

//Check for excluded HDMI-CEC hardware
#if !defined(SELECTED_CHIP_HAS_CEC) 
    #define SELECTED_CHIP_HAS_CEC         false
#endif

//Check for excluded Serial Audio Interface hardware
#if !defined(SELECTED_CHIP_HAS_SAI1) 
    #define SELECTED_CHIP_HAS_SAI1         false
#endif
#if !defined(SELECTED_CHIP_HAS_SAI2) 
    #define SELECTED_CHIP_HAS_SAI2         false
#endif
#if !defined(SELECTED_CHIP_HAS_SAI3) 
    #define SELECTED_CHIP_HAS_SAI3         false
#endif
#if !defined(SELECTED_CHIP_HAS_SAI4) 
    #define SELECTED_CHIP_HAS_SAI4         false
#endif

//Check for excluded SPDIF hardware
#if !defined(SELECTED_CHIP_HAS_SPDIFRX) 
    #define SELECTED_CHIP_HAS_SPDIFRX         false
#endif

//Check for excluded Delay Block hardware
#if !defined(SELECTED_CHIP_HAS_DLYB) 
    #define SELECTED_CHIP_HAS_DLYB         false
#endif

//Check for excluded Digital Camera Interface hardware
#if !defined(SELECTED_CHIP_HAS_DCMI) 
    #define SELECTED_CHIP_HAS_DCMI         false
#endif

//Check for excluded JPEG image processing hardware
#if !defined(SELECTED_CHIP_HAS_JPEG) 
    #define SELECTED_CHIP_HAS_JPEG         false
#endif

//Check for excluded LCD/TFT display driver hardare
#if !defined(SELECTED_CHIP_HAS_LTDC) 
    #define SELECTED_CHIP_HAS_LTDC         false
#endif

//Check for excluded Random Number Generator hardware
#if !defined(SELECTED_CHIP_HAS_RNG) 
    #define SELECTED_CHIP_HAS_RNG         false
#endif

//Check for excluded Cryptographic Accelerator hardware
#if !defined(SELECTED_CHIP_HAS_CRYP) 
    #define SELECTED_CHIP_HAS_CRYP         false
#endif

//Check for excluded Hash Processor hardware
#if !defined(SELECTED_CHIP_HAS_HASH) 
    #define SELECTED_CHIP_HAS_HASH         false
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __STM32F0xx_H */