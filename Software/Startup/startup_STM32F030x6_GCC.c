
/**
 ******************************************************************************
 * @file      startup_STM32F301x8_Gnu
 * @author    Izzy
 * @version   V1.0
 * @date      01/17/2019
 * @brief     N572 devices startup code.
 *            This module performs:
 *                - Set the initial SP
 *                - Set the vector table entries with the exceptions ISR address
 *                - Initialize data and bss
 *                - Setup the microcontroller system.
 *                - Call the application's entry point.
 *            After Reset the Cortex-M4 processor is in Thread mode,
 *            priority is Privileged, and the Stack is set to Main.
 *******************************************************************************
 */
#include <stdint.h>

/*----------Macro definition--------------------------------------------------*/
#define WEAK __attribute__ ((weak))

/*----------Declaration of the default fault handlers-------------------------*/
/* System exception vector handler */
__attribute__ ((used))
//Core exceptions
void WEAK  Reset_Handler(void);
void WEAK  NMI_Handler(void);
void WEAK  HardFault_Handler(void);
void WEAK  SVC_Handler(void);
void WEAK  PendSV_Handler(void);
void WEAK  SysTick_Handler(void);

//IRQs 0-3
void WEAK  WWDG_IRQHandler(void);
void WEAK  RTC_IRQHandler(void);
void WEAK  FLASH_IRQHandler(void);
//IRQs 4-7
void WEAK  RCC_IRQHandler(void);
void WEAK  EXTI0_1_IRQHandler(void);
void WEAK  EXTI2_3_IRQHandler(void);
void WEAK  EXTI4_15_IRQHandler(void);
//IRQs 8-11
void WEAK  DMA1_Channel1_IRQHandler(void);
void WEAK  DMA1_Channel2_3_IRQHandler(void);
void WEAK  DMA1_Channel4_5_IRQHandler(void);
//IRQs 12-15
void WEAK  ADC1_IRQHandler(void);
void WEAK  TIM1_BRK_UP_TRG_COM_IRQHandler(void);
void WEAK  TIM1_CC_IRQHandler(void);
//IRQs 16-19
void WEAK  TIM3_IRQHandler(void);
void WEAK  TIM14_IRQHandler(void);
//IRQs 20-23
void WEAK  TIM16_IRQHandler(void);
void WEAK  TIM17_IRQHandler(void);
void WEAK  I2C1_IRQHandler(void);
//IRQs 24-27
void WEAK  SPI1_IRQHandler(void);
void WEAK  USART1_IRQHandler(void);
//IRQs 28-31


/*----------Symbols defined in linker script----------------------------------*/
extern uint32_t _start_ram;
extern uint32_t _eram;
extern uint32_t __etext;
extern uint32_t __data_start__;
extern uint32_t __data_end__;
extern uint32_t __bss_start__;
extern uint32_t __bss_end__;
extern uint32_t __StackTop;

typedef void( *pFunc )( void );

/*----------Function prototypes-----------------------------------------------*/
extern int main(void);           /*!< The entry point for the application */
void Default_Reset_Handler(void);   /*!< Default reset handler */
static void Default_Handler(void);  /*!< Default exception handler */

/**
  *@brief The minimal vector table for a Cortex M3.  Note that the proper constructs
  *       must be placed on this to ensure that it ends up at physical address
  *       0x00000000.
  */
const pFunc __Vectors[] __attribute__ ((section(".vectors"))) = {
  /*----------Core Exceptions------------------------------------------------ */
  (pFunc)((uint32_t)&__StackTop),      /*!< The initial stack pointer         */
  Reset_Handler,                       /*!< The reset handler                 */
  NMI_Handler,                         /*!< The NMI handler                   */
  HardFault_Handler,                   /*!< The hard fault handler            */
  Default_Handler,                     /*!< Memory Management Fault           */
  Default_Handler,                     /*!< Bus Arbitration Fault                          */
  Default_Handler,                     /*!< Reserved                          */
  Default_Handler,                     /*!< Reserved                          */
  Default_Handler,                     /*!< Reserved                          */
  Default_Handler,                     /*!< Reserved                          */
  Default_Handler,                     /*!< Reserved                          */
  SVC_Handler,                         /*!< SVCall handler                    */
  Default_Handler,                     /*!< Reserved                          */
  Default_Handler,                     /*!< Reserved                          */
  PendSV_Handler,                      /*!< The PendSV handler                */
  SysTick_Handler,                     /*!< The SysTick handler               */

  /*----------External Exceptions---------------------------------------------*/
  //0-3
 WWDG_IRQHandler,
 Default_Handler,
 RTC_IRQHandler,
 FLASH_IRQHandler,
 //4-7
 RCC_IRQHandler,
 EXTI0_1_IRQHandler,
 EXTI2_3_IRQHandler,
 EXTI4_15_IRQHandler,
 //8-11
 Default_Handler,
 DMA1_Channel1_IRQHandler,
 DMA1_Channel2_3_IRQHandler,
 DMA1_Channel4_5_IRQHandler,
 //12-15
 ADC1_IRQHandler,
 TIM1_BRK_UP_TRG_COM_IRQHandler,
 TIM1_CC_IRQHandler,
 Default_Handler,
 //16-19
 TIM3_IRQHandler,
 Default_Handler,
 Default_Handler,
 TIM14_IRQHandler,
 //20-23
 Default_Handler,
 TIM16_IRQHandler,
 TIM17_IRQHandler,
 I2C1_IRQHandler,
 //24-27
 Default_Handler,
 SPI1_IRQHandler,
 Default_Handler,
 USART1_IRQHandler,
 //28-31
 Default_Handler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
};


uint32_t randomSeed __attribute__((section(".uninit")));

/**
  * @brief  This is the code that gets called when the processor first
  *         starts execution following a reset event. Only the absolutely
  *         necessary set is performed, after which the application
  *         supplied main() routine is called.
  * @param  None
  * @retval None
  */
void Default_Reset_Handler(void)
{
    uint32_t *pSrc, *pDest;
    uint32_t *pTable __attribute__((unused));

    //Go generate a random seed...
    //XOR in all the stuff before the "uninitialized" section
    pSrc = &_start_ram;
    for( ; pSrc < &__data_end__;pSrc++) {
        randomSeed ^= (*pSrc);
    }
    //And XOR in all the stuff after the "uninitialized" section.
    pSrc = &__bss_start__;
    for( ; pSrc < &_eram;pSrc++) {
        randomSeed ^= (*pSrc);
    }

    /*  Single section scheme.
     *
     *  The ranges of copy from/to are specified by following symbols
     *    __etext: LMA of start of the section to copy from. Usually end of text
     *    __data_start__: VMA of start of the section to copy to
     *    __data_end__: VMA of end of the section to copy to
     *
     *  All addresses must be aligned to 4 bytes boundary.
     */
    pSrc  = &__etext;
    pDest = &__data_start__;

    for ( ; pDest < &__data_end__ ; ) {
    *pDest++ = *pSrc++;
    }
    /*  Single BSS section scheme.
    *
    *  The BSS section is specified by following symbols
    *    __bss_start__: start of the BSS section.
    *    __bss_end__: end of the BSS section.
    *
    *  Both addresses must be aligned to 4 bytes boundary.
    */
    pDest = &__bss_start__;

    for ( ; pDest < &__bss_end__ ; ) {
      *pDest++ = 0ul;
    }

  /* Call the application's entry point.*/
  main();

  //Go into an infinite loop if main() returns
  Default_Handler();
}


/**
  *@brief Provide weak aliases for each Exception handler to the Default_Handler.
  *       As they are weak aliases, any function with the same name will override
  *       this definition.
  */

#pragma weak Reset_Handler = Default_Reset_Handler
#pragma weak NMI_Handler = Default_Handler
#pragma weak HardFault_Handler = Default_Handler
#pragma weak SVC_Handler = Default_Handler
#pragma weak PendSV_Handler = Default_Handler
#pragma weak SysTick_Handler = Default_Handler

#pragma weak WWDG_IRQHandler = Default_Handler
#pragma weak RTC_IRQHandler = Default_Handler
#pragma weak FLASH_IRQHandler = Default_Handler

#pragma weak RCC_IRQHandler = Default_Handler
#pragma weak EXTI0_1_IRQHandler = Default_Handler
#pragma weak EXTI2_3_IRQHandler = Default_Handler
#pragma weak EXTI4_15_IRQHandler = Default_Handler

#pragma weak DMA1_Channel1_IRQHandler = Default_Handler
#pragma weak DMA1_Channel2_3_IRQHandler = Default_Handler
#pragma weak DMA1_Channel4_5_IRQHandler = Default_Handler

#pragma weak ADC1_IRQHandler = Default_Handler
#pragma weak TIM1_BRK_UP_TRG_COM_IRQHandler = Default_Handler
#pragma weak TIM1_CC_IRQHandler = Default_Handler

#pragma weak TIM3_IRQHandler = Default_Handler
#pragma weak TIM14_IRQHandler = Default_Handler

#pragma weak TIM16_IRQHandler = Default_Handler
#pragma weak TIM17_IRQHandler = Default_Handler
#pragma weak I2C1_IRQHandler = Default_Handler

#pragma weak SPI1_IRQHandler = Default_Handler
#pragma weak USART1_IRQHandler = Default_Handler


/**
  * @brief  This is the code that gets called when the processor receives an
  *         unexpected interrupt.  This simply enters an infinite loop,
  *         preserving the system state for examination by a debugger.
  * @param  None
  * @retval None
  */
static void Default_Handler(void)
{
  /* Go into an infinite loop. */
  while (1)
  {
  }
}
